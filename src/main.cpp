// Includes requeridos para el main
#include <iostream>
#include <fstream> //Para .CSV
#include "arbol.h"
#include <time.h>
#include <unistd.h>
// Includes requeridos para los experimentos
#include <chrono>
#include <vector>
#include <set>
#include <unordered_set>
#include <algorithm>
// Metodo para generar el CSV
void writeCSV(char *name, int *op, int *val, double *time, int batches);

int main(int argc, const char *argv[])
{

    // el primer parametro define cuantas pruebas se haran para cada experimento
    int batches = std::stoi(argv[1]);
    // el segundo parametro define de cuantas operaciones consiste cada prueba
    int batchSize = std::stoi(argv[2]);

    // DECLARACIONES
    // Almacenamiento de resultados
    double *tiempos = new double[1000];
    int *operacion = new int[1000];
    int *valor = new int[1000];

    // Estructuras de datos
    PTree<int> *prueba_a = new PTree<int>;             // Prueba BST
    std::vector<int> *prueba_b = new std::vector<int>; // Prueba vector
    std::vector<int>::iterator iter_v;
    std::set<int> *prueba_c = new std::set<int>; // Prueba Set
    std::set<int>::iterator iter_s;
    std::unordered_set<int> *prueba_d = new std::unordered_set<int>; // Prueba unordered_set

    // Variables para medicion del tiempo
    std::chrono::high_resolution_clock::time_point t1;
    std::chrono::high_resolution_clock::time_point t2;
    std::chrono::duration<double, std::nano> time;

    // Pruebas unitarias

    std::cout << "Se hace un arbol vacio y se le insertan los valores 8 - 4 - 5 - 11 en ese orden" << std::endl;

    prueba_a->Insert(new node<int>, 8);
    prueba_a->Insert(new node<int>, 4);
    prueba_a->Insert(new node<int>, 5);
    prueba_a->Insert(new node<int>, 11);

    for (int i = 3; i < 13; i++)
    {
        if (prueba_a->Contains(prueba_a->raiz, i))
        {
            std::cout << "El arbol contiene " << i << std::endl;
        }
        else
        {
            std::cout << "El arbol no contiene " << i << std::endl;
        }
    }

    std::cout << "recorrido postorden" << std::endl;

    std::cout << prueba_a->raiz->left->right->value << " ";
    std::cout << prueba_a->raiz->left->value << " ";
    std::cout << prueba_a->raiz->right->value << " ";
    std::cout << prueba_a->raiz->value << std::endl;

    std::cout << std::endl
              << "Se iniciaran las pruebas" << std::endl;
    sleep(10);

    delete prueba_a;
    prueba_a = new PTree<int>;

    // PRUEBAS DE BST
    
    // Prueba de INSERCION EN ORDEN - CADA 1000 INSERCIONES
    for (int i = 0; i < batches; i++)
    {
        t1 = std::chrono::high_resolution_clock::now();
        for (int j = 0; j < batchSize; j++)
        {
            prueba_a->Insert(new node<int>, j + 1000 * i);
        }
        t2 = std::chrono::high_resolution_clock::now();
        operacion[i] = i;
        valor[i] = i;
        time = t2 - t1;
        tiempos[i] = time.count();
        std::cout << std::fixed << time.count() << std::endl;
    }

    char nombreBST1[] = "bst_in_or.csv";
    writeCSV(nombreBST1, operacion, valor, tiempos, batches);
    

    // Prueba de INSERCION AL AZAR - CADA 1000 INSERCIONES
    // Borramos el puntero prueba_a y lo reutilizamos
    delete prueba_a;
    prueba_a = new PTree<int>;
    for (int i = 0; i < batches; i++)
    {
        valor[i] = rand();
        t1 = std::chrono::high_resolution_clock::now();
        for (int j = 0; j < batchSize; j++)
        {
            prueba_a->Insert(new node<int>, valor[i]);
        }
        t2 = std::chrono::high_resolution_clock::now();
        operacion[i] = i;
        time = t2 - t1;
        tiempos[i] = time.count();
        std::cout << std::fixed << time.count() << std::endl;
    }
    char nombreBST2[] = "bst_in_az.csv";
    writeCSV(nombreBST2, operacion, valor, tiempos, batches);

    // Prueba de CONTIENE EN ORDEN - CADA 1000 INSERCIONES
    // Vamos a usar el arbol de INSERCION AL AZAR con 1mill de datos al azar para esta prueba :)
    for (int i = 0; i < batches; i++)
    {
        t1 = std::chrono::high_resolution_clock::now();
        for (int j = 0; j < batchSize; j++)
        {
            prueba_a->Contains(prueba_a->raiz, j + 1000 * i);
        }
        t2 = std::chrono::high_resolution_clock::now();
        operacion[i] = i;
        valor[i] = i;
        time = t2 - t1;
        tiempos[i] = time.count();
        std::cout << std::fixed << time.count() << std::endl;
    }
    char nombreBST3[] = "bst_con_or.csv";
    writeCSV(nombreBST3, operacion, valor, tiempos, batches);

    // Prueba de CONTIENE AL AZAR
    // Debemos eliminar prueba_a y reutilizarlo solo con 1000 datos al azar :)
    delete prueba_a;
    prueba_a = new PTree<int>;
    // FOR INSERTAR DATOS
    for (int i = 0; i < 1000; i++)
    {
        prueba_a->Insert(new node<int>, rand());
    }
    // FOR DE CONTAINS AZAR
    for (int i = 0; i < batches; i++)
    {
        valor[i] = rand();
        t1 = std::chrono::high_resolution_clock::now();
        for (int j = 0; j < batchSize; j++)
        {
            prueba_a->Contains(prueba_a->raiz, valor[i]);
        }
        t2 = std::chrono::high_resolution_clock::now();
        operacion[i] = i;
        time = t2 - t1;
        tiempos[i] = time.count();
        std::cout << std::fixed << time.count() << std::endl;
    }

    char nombreBST4[] = "bst_con_az.csv";
    writeCSV(nombreBST4, operacion, valor, tiempos, batches);

    ///////////////////////////////////////////////////////////////
    // PRUEBAS DE STD::VECTOR

    // Prueba de INSERCION EN ORDEN - CADA 1000 INSERCIONES
    iter_v = prueba_b->begin();
    for (int i = 0; i < batches; i++)
    {
        t1 = std::chrono::high_resolution_clock::now();
        for (int j = 0; j < batchSize; j++)
        {
            iter_v = prueba_b->insert(iter_v, j + i * 1000);
        }
        t2 = std::chrono::high_resolution_clock::now();
        operacion[i] = i;
        valor[i] = i;
        time = t2 - t1;
        tiempos[i] = time.count();
        std::cout << std::fixed << time.count() << std::endl;
    }

    char nombreVEC1[] = "vec_in_or.csv";
    writeCSV(nombreVEC1, operacion, valor, tiempos, batches);

    // Prueba de INSERCION AL AZAR - CADA 1000 INSERCIONES
    // Borramos el puntero prueba_b y lo reutilizamos
    delete prueba_b;
    prueba_b = new std::vector<int>;
    iter_v = prueba_b->begin();
    for (int i = 0; i < batches; i++)
    {
        valor[i] = rand();
        t1 = std::chrono::high_resolution_clock::now();
        for (int j = 0; j < batchSize; j++)
        {
            iter_v = prueba_b->insert(iter_v, valor[i]);
        }
        t2 = std::chrono::high_resolution_clock::now();
        operacion[i] = i;
        time = t2 - t1;
        tiempos[i] = time.count();
        std::cout << std::fixed << time.count() << std::endl;
    }
    char nombreVEC2[] = "vec_in_az.csv";
    writeCSV(nombreVEC2, operacion, valor, tiempos, batches);

    // Prueba de CONTIENE EN ORDEN - CADA 1000 INSERCIONES
    // Vamos a usar el arbol de INSERCION AL AZAR con 1mill de datos al azar para esta prueba :)
    for (int i = 0; i < batches; i++)
    {
        t1 = std::chrono::high_resolution_clock::now();
        for (int j = 0; j < batchSize; j++)
        {
            std::find(prueba_b->begin(), prueba_b->end(), j + i * 1000);
        }
        t2 = std::chrono::high_resolution_clock::now();
        operacion[i] = i;
        valor[i] = i;
        time = t2 - t1;
        tiempos[i] = time.count();
        std::cout << std::fixed << time.count() << std::endl;
    }
    char nombreVEC3[] = "vec_con_or.csv";
    writeCSV(nombreVEC3, operacion, valor, tiempos, batches);

    // Prueba de CONTIENE AL AZAR
    // Debemos eliminar prueba_b y reutilizarlo solo con 1000 datos al azar :)
    delete prueba_b;
    prueba_b = new std::vector<int>;
    iter_v = prueba_b->begin();
    // FOR INSERTAR DATOS
    for (int i = 0; i < 1000; i++)
    {
        iter_v = prueba_b->insert(iter_v, rand());
    }
    // FOR DE CONTAINS AZAR
    for (int i = 0; i < batches; i++)
    {
        valor[i] = rand();
        t1 = std::chrono::high_resolution_clock::now();
        for (int j = 0; j < batchSize; j++)
        {
            std::find(prueba_b->begin(), prueba_b->end(), valor[i]);
        }
        t2 = std::chrono::high_resolution_clock::now();
        operacion[i] = i;
        time = t2 - t1;
        tiempos[i] = time.count();
        std::cout << std::fixed << time.count() << std::endl;
    }

    char nombreVEC4[] = "vec_con_az.csv";
    writeCSV(nombreVEC4, operacion, valor, tiempos, batches);

    ///////////////////////////////////////////////////////////////
    // PRUEBAS DE STD::SET

    // Prueba de INSERCION EN ORDEN - CADA 1000 INSERCIONES
    iter_s = prueba_c->begin();
    for (int i = 0; i < batches; i++)
    {
        t1 = std::chrono::high_resolution_clock::now();
        for (int j = 0; j < batchSize; j++)
        {
            prueba_c->insert(j + i * 1000);
        }
        t2 = std::chrono::high_resolution_clock::now();
        operacion[i] = i;
        valor[i] = i;
        time = t2 - t1;
        tiempos[i] = time.count();
        std::cout << std::fixed << time.count() << std::endl;
    }
    char nombreSET1[] = "set_in_or.csv";
    writeCSV(nombreSET1, operacion, valor, tiempos, batches);

    // Prueba de INSERCION AL AZAR - CADA 1000 INSERCIONES
    // Borramos el puntero prueba_c y lo reutilizamos
    delete prueba_c;
    prueba_c = new std::set<int>;
    iter_s = prueba_c->begin();
    for (int i = 0; i < batches; i++)
    {
        valor[i] = rand();
        t1 = std::chrono::high_resolution_clock::now();
        for (int j = 0; j < batchSize; j++)
        {
            prueba_c->insert(valor[i]);
        }
        t2 = std::chrono::high_resolution_clock::now();
        operacion[i] = i;
        time = t2 - t1;
        tiempos[i] = time.count();
        std::cout << std::fixed << time.count() << std::endl;
    }

    char nombreSET2[] = "set_in_az.csv";
    writeCSV(nombreSET2, operacion, valor, tiempos, batches);

    // Prueba de CONTIENE EN ORDEN - CADA 1000 INSERCIONES
    // Vamos a usar el arbol de INSERCION AL AZAR con 1mill de datos al azar para esta prueba :)
    for (int i = 0; i < batches; i++)
    {
        t1 = std::chrono::high_resolution_clock::now();
        for (int j = 0; j < batchSize; j++)
        {
            prueba_c->find(j + i * 1000);
        }
        t2 = std::chrono::high_resolution_clock::now();
        operacion[i] = i;
        valor[i] = i;
        time = t2 - t1;
        tiempos[i] = time.count();
        std::cout << std::fixed << time.count() << std::endl;
    }

    char nombreSET3[] = "set_con_or.csv";
    writeCSV(nombreSET3, operacion, valor, tiempos, batches);

    // Prueba de CONTIENE AL AZAR
    // Debemos eliminar prueba_c y reutilizarlo solo con 1000 datos al azar :)
    delete prueba_c;
    prueba_c = new std::set<int>;
    iter_s = prueba_c->begin();
    // FOR INSERTAR DATOS
    for (int i = 0; i < 1000; i++)
    {
        prueba_c->insert(rand());
    }
    // FOR DE CONTAINS AZAR
    for (int i = 0; i < batches; i++)
    {
        valor[i] = rand();
        t1 = std::chrono::high_resolution_clock::now();
        for (int j = 0; j < batchSize; j++)
        {
            prueba_c->find(valor[i]);
        }
        t2 = std::chrono::high_resolution_clock::now();
        operacion[i] = i;
        time = t2 - t1;
        tiempos[i] = time.count();
        std::cout << std::fixed << time.count() << std::endl;
    }

    char nombreSET4[] = "set_con_az.csv";
    writeCSV(nombreSET4, operacion, valor, tiempos, batches);

    ///////////////////////////////////////////////////////////////
    // PRUEBAS DE STD::UNORDERED_SET

    // Prueba de INSERCION EN ORDEN - CADA 1000 INSERCIONES
    for (int i = 0; i < batches; i++)
    {
        t1 = std::chrono::high_resolution_clock::now();
        for (int j = 0; j < batchSize; j++)
        {
            prueba_d->insert(j + 1000 * i);
        }
        t2 = std::chrono::high_resolution_clock::now();
        operacion[i] = i;
        valor[i] = i;
        time = t2 - t1;
        tiempos[i] = time.count();
        std::cout << std::fixed << time.count() << std::endl;
    }

    char nombreUNSET1[] = "unset_in_or.csv";
    writeCSV(nombreUNSET1, operacion, valor, tiempos, batches);

    // Prueba de INSERCION AL AZAR - CADA 1000 INSERCIONES
    // Borramos el puntero prueba_a y lo reutilizamos
    delete prueba_d;
    prueba_d = new std::unordered_set<int>;
    for (int i = 0; i < batches; i++)
    {
        valor[i] = rand();
        t1 = std::chrono::high_resolution_clock::now();
        for (int j = 0; j < batchSize; j++)
        {
            prueba_d->insert(valor[i]);
        }
        t2 = std::chrono::high_resolution_clock::now();
        operacion[i] = i;
        time = t2 - t1;
        tiempos[i] = time.count();
        std::cout << std::fixed << time.count() << std::endl;
    }

    char nombreUNSET2[] = "unset_in_az.csv";
    writeCSV(nombreUNSET2, operacion, valor, tiempos, batches);

    // Prueba de CONTIENE EN ORDEN - CADA 1000 INSERCIONES
    // Vamos a usar el arbol de INSERCION AL AZAR con 1mill de datos al azar para esta prueba :)
    for (int i = 0; i < batches; i++)
    {
        t1 = std::chrono::high_resolution_clock::now();
        for (int j = 0; j < batchSize; j++)
        {
            prueba_d->find(j + 1000 * i);
        }
        t2 = std::chrono::high_resolution_clock::now();
        operacion[i] = i;
        valor[i] = i;
        time = t2 - t1;
        tiempos[i] = time.count();
        std::cout << std::fixed << time.count() << std::endl;
    }

    char nombreUNSET3[] = "unset_con_or.csv";
    writeCSV(nombreUNSET3, operacion, valor, tiempos, batches);

    // Prueba de CONTIENE AL AZAR
    // Debemos eliminar prueba_a y reutilizarlo solo con 1000 datos al azar :)
    delete prueba_d;
    prueba_d = new std::unordered_set<int>;
    // FOR INSERTAR DATOS
    for (int i = 0; i < 1000; i++)
    {
        prueba_d->insert(rand());
    }
    // FOR DE CONTAINS AZAR
    for (int i = 0; i < batches; i++)
    {
        valor[i] = rand();
        t1 = std::chrono::high_resolution_clock::now();
        for (int j = 0; j < batchSize; j++)
        {
            prueba_d->find(valor[i]);
        }
        t2 = std::chrono::high_resolution_clock::now();
        operacion[i] = i;
        time = t2 - t1;
        tiempos[i] = time.count();
        std::cout << std::fixed << time.count() << std::endl;
    }

    char nombreUNSET4[] = "unset_con_az.csv";
    writeCSV(nombreUNSET4, operacion, valor, tiempos, batches);

    delete[] tiempos;
    delete[] operacion;
    delete[] valor;
    delete prueba_a;
    delete prueba_b;
    delete prueba_c;
    delete prueba_d;

    return 0;
}

void writeCSV(char *name, int *op, int *val, double *time, int batches)
{

    std::fstream registro;
    registro.open(name, std::ios::out | std::ios::app);

    registro << "operacion";

    for (int i = 0; i < batches; i++)
    {
        registro << "," << op[i];
    }

    registro << "\n"
             << "valor";

    for (int i = 0; i < batches; i++)
    {
        registro << "," << val[i];
    }

    registro << "\n"
             << "tiempo";

    for (int i = 0; i < batches; i++)
    {
        registro << "," << std::fixed << time[i];
    }

    registro.close();
}