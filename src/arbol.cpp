// Aqui se encuentran las funciones de "arbol.h"
#include "arbol.h"

// Template de clase "node"
template <typename T>
// Constructor
node<T>::node()
{
    //"this" se asegura de que se haga el cambio a la variable del nodo que es
    this->parent = nullptr;
    this->left = nullptr;
    this->right = nullptr;
};

template <typename T>
// Destructor
node<T>::~node(){};

// Template de clase "PTree"
template <typename T>
// Constructor
PTree<T>::PTree()
{
    this->raiz = nullptr;
}

template <typename T>
// Destructor
PTree<T>::~PTree()
{
    deleteTree(this->raiz);
}

template <typename T>
void PTree<T>::deleteTree(node<T> *z)
{
    if (z == nullptr)
    {
        return;
    }

    deleteTree(z->left);
    deleteTree(z->right);

    delete z;
}

template <typename T>
void PTree<T>::Insert(node<T> *clave, T valor)
{
    clave->value = valor;
    if (this->raiz == nullptr)
    {
        this->raiz = clave;
        return;
    }

    node<T> *temp = this->raiz;
    while (true)
    {
        if (clave->value < temp->value)
        {

            if (temp->left != nullptr)
            {
                temp = temp->left;
            }
            else
            {
                temp->left = clave;
                clave->parent = temp;
                break;
            }
        }
        else
        { //  Si es igual o mayor va a la derecha

            if (temp->right != nullptr)
            {
                temp = temp->right;
            }
            else
            {
                temp->right = clave;
                clave->parent = temp;
                break;
            }
        }
    }
}

template <typename T>
bool PTree<T>::Contains(node<T> *clave, T valor)
{
    if (this->raiz == nullptr)
    {
        return false;
    }

    node<T> *temp = raiz;
    while (true)
    {
        if (temp->value == valor)
        {
            return true;
        }

        if (valor < temp->value)
        {
            if (temp->left != nullptr)
            {
                temp = temp->left;
            }
            else
            {
                return false;
            }
        }
        else
        { //  Si es igual o mayor va a la derecha

            if (temp->right != nullptr)
            {
                temp = temp->right;
            }
            else
            {
                return false;
            }
        }
    }
}
template class PTree<int>;
template class node<int>;