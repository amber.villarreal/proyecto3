//Define la clase arbol si no esta definida. Si ya lo esta no hace nada
#ifndef ARBOL_H
#define ARBOL_H
//Declaracion de metodos y variables
#include <iostream>

// Template de clase "node"
template <typename T>
class node
{
public:
    node<T> *parent, *left, *right;
    T value;

    // Constructor
    node();

    // Destructor
    ~node();
};

// Template de clase "Tree"
template <typename T>
class PTree
{

public:
    node<T> *raiz;

    // Constructor
    PTree();

    // Destructor
    ~PTree();

    //Eliminar arbol
    void deleteTree(node<T> *z);

    // Metodo insertar nuevo valor
    void Insert(node<T> *clave, T valor);

    // Revisa que el valor de clave ya este en el arbol. Retorna true si esta y false si no
    bool Contains(node<T> *clave, T valor);
    
};

#endif //ARBOL.H