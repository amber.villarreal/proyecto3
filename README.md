# Proyecto3
Universidad de Costa Rica
Escuela de Ciencias de la Computacion
Programacion 2 - CI-0113
Grupo 2
Profesor: Esteban Rodriguez Betancourt 
Estudiante: Amber Villarreal Campos C28481

# Compilacion del proyecto
Para compilar este proyecto debe tener CMake instalado en su dispositivo.

# Instrucciones
- Abrir terminal de la carpeta del proyecto 3
- Ponga en la terminal las siguientes dos lineas:
cmake -S . -B build
cmake --build build
- Luego, a la hora de correr el proyecto, escriba en la terminal:
./build/Proyecto3 (batch) (batches)
- En lugar de (batch) y (batches) ponga la cantidad de pruebas que quiera hacer y la cantidad de números que quiere que se corran por prueba.
Entonces por ejemplo: ./build/Proyecto3 1000 1000
Hara mil pruebas de mil numeros cada una. Es decir, correra 1 millon de numeros en grupos de mil por operacion.